# Submit New order.


**URL** : `/api/submit`

**Method** : `POST`

**Data constraints**

all fields required 

**Data example:** All fields must be sent.

```json
{
    "CountryCode": 1
    "Number": "431513412",
    "Lat": 47.60621,
    "Lon": -122.332071,
    "Email" "test@gmail.com"
}
```


## Success Response

**Condition** : If it success, it will send a success response

**Code** : `200 OK`

**Content example**

```
Sms code sent
```

## Error Responses

**Condition** : If there is some issue

**Code** : `500 InternalServerError`
